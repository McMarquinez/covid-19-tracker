const totalCounters = "https://corona.lmao.ninja/all";
const worldCounter = "https://corona.lmao.ninja/countries";

var cases = $('#ttlInfected');
var deaths = $('#ttlDeaths');
var recovered = $('#ttlRecovered');
var active = $('#ttlActive');
var update = $('#lastUpdate');
var tblCountries = $('#tblCountries');



$( document ).ready(function() {
    $.getJSON(totalCounters, function(result){
        // console.log(result.cases);
        cases.html(numberWithCommas(result.cases));
        deaths.html(numberWithCommas(result.deaths));
        recovered.html(numberWithCommas(result.recovered));
        active.html(numberWithCommas(result.active));
        update.html("Last Update: " + convertDate(result.updated));
    });
    $.getJSON(worldCounter, function(result){
        var i;
        for (i = 0; i < result.length; i++) {

            // var country = result[i].countryInfo.iso2;
            // eval('svgMapDataMC.' + country + '= {infected:'+result[i].cases+',recovered:'+result[i].recovered+',deaths:'+result[i].deaths+',casePerMillion:'+result[i].casesPerOneMillion+'}');
            
            // console.log(result[i].country);
            tblCountries.append('<tr><td><img style=height:15px src="'+result[i].countryInfo.flag+'"> '+ result[i].country +'</td><td>'+numberWithCommas(result[i].cases)+'</td><td>'+result[i].casesPerOneMillion+'</td><td>'+ numberWithCommas(result[i].recovered) +'</td><td>'+ numberWithCommas(result[i].deaths) +'</td></tr>')
        }
    });
});
var statistics = {
    data: {
      infected: {
        name: 'Infected',
        thousandSeparator: ',',
        thresholdMax: 2000,
        thresholdMin: 1,

      },
      recovered: {
        name: 'Recovered',
        thousandSeparator: ',',
      },
      deaths: {
        name: 'Deaths',
        thousandSeparator: ',',
      },
      casePerMillion: {
        name: 'Cases Per 1 Million',
        thousandSeparator: ',',
      }
    },
    applyData: 'infected',
    values : getData(),
    
  }

new svgMap({
    targetElementID: 'svgMapGPD',
    data: statistics,
    // Data colors
    colorMax: '#ed6655',
    colorMin: '#b1af5d',
    colorNoData: '#ed6655',

});


function getData() {
    var svgMapDataMC = {};
    $.getJSON(worldCounter, function(result){
        var i;
        for (i = 0; i < result.length; i++) {
            var country = result[i].countryInfo.iso2;
            eval('svgMapDataMC.' + country + '= {infected:'+result[i].cases+',recovered:'+result[i].recovered+',deaths:'+result[i].deaths+',casePerMillion:'+result[i].casesPerOneMillion+'}');
        }
    });
    return svgMapDataMC;
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
}

function convertDate(date){   
    const dateObject = new Date(date)
    const humanDateFormat = dateObject.toLocaleString() //2019-12-9 10:30:15

    return humanDateFormat;
}