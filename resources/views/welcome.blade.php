@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row text-white">
        <div class="col-md-12">
          <h1>World Covid-19 Tracker</h1>
          <strong id="lastUpdate">Last Update:</strong>

          <hr>
          <div class="row mb-4">
              <div class="col-sm-3 col-6">
                <div class="card bg-dark border-0 border-dark shadow">
                  <div class="card-header">
                    <div class="header-counter text-warning">
                      <h1 class="card-title"><i class="fas fa-users"></i><br> <span id="ttlInfected" class="count"></span></h1>
                      <span>TOTAL INFECTED</span>
                    </div>
                  </div>
                </div>
                <!-- /.-->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-6">
                <div class="card bg-dark border-0 border-dark shadow">
                  <div class="card-header">
                    <div class="header-counter text-danger">
                      <h1 class="card-title"><i class="fas fa-skull-crossbones"></i><br> <span id="ttlDeaths" class="count"></span></h1>
                      <span>DEATHS</span>
                    </div>
                  </div>
                </div>
                <!-- /.-->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-6">
                <div class="card bg-dark border-0 border-dark shadow">
                  <div class="card-header">
                    <div class="header-counter text-success">
                      <h1 class="card-title"><i class="fas fa-clinic-medical"></i><br> <span id="ttlRecovered" class="count"></span></h1>
                      <span>RECOVERED</span>
                    </div>
                  </div>
                </div>
                <!-- /.-->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-6">
                <div class="card bg-dark border-0 border-dark shadow">
                  <div class="card-header">
                    <div class="header-counter text-white">
                      <h1 class="card-title"><i class="fas fa-procedures"></i><br> <span id="ttlActive" class="count"></span></h1>
                      <span>ACTIVE</span>
                    </div>
                  </div>
                </div>
                <!-- /.-->
              </div>
          </div> 
          <div class="row mb-4">
            <div class="col-md-12">
              <div class="card bg-dark border-0 border-dark shadow">
                <div class="card-body">
                  <div id="svgMapGPD"></div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card border-0 bg-dark border-dark shadow">
                <div class="card-header">
                    <div class="float-right input-group col-md-3 mb-3">
                      <input type="text" class="form-control" placeholder="Search" style="background-color:transparent; border-radius:0px; color: white">
                    </div>
                </div>
                <div class="card-body p-0 country-container">
                    <table class="table table-hover table-countries text-white">
                      <thead>
                        <tr>
                          <th>Location</th>
                          <th>Confirmed</th>
                          <th>Cases Per 1 Million</th>
                          <th>Recovered</th>
                          <th>Deaths</th>
                        </tr>
                      </thead>
                      <tbody id="tblCountries">
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="col-md-12 mt-5">
          <h3>About this data</h3>
          <p>This data changes rapidly, so what’s shown may be out of date. Table totals may not always represent an accurate sum. Information about reported cases is also available on the <a href="https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports">World Health Organization site</a>.</p>
          <p><b>Source:</b> <a href="https://github.com/NovelCOVID/API">NovelCOVID</a></p>
        </div>
    </div>
</div>
@endsection
