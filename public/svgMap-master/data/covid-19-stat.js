var statistics = {
  data: {
    infected: {
      name: 'Infected',
      thousandSeparator: ',',
    },
    recovered: {
      name: 'Recovered',
      thousandSeparator: ',',
    },
    deaths: {
      name: 'Deaths',
      thousandSeparator: ',',
    },
    casePerMillion: {
      name: 'Cases Per 1 Million',
      thousandSeparator: ',',
    }
  },
  applyData: 'deaths',
  values: svgMapDataMC,
}